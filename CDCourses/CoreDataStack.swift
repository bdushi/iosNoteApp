//
//  CoreDataStack.swift
//  CDCourses
//
//  Created by 1sd on 1/22/18.
//  Copyright © 2018 isd. All rights reserved.
//

import Foundation
import CoreData

func createMainContext() -> NSManagedObjectContext{
    //Initialize NsManageObjectModel
    let modelURL = Bundle.main.url(forResource: "CDCourses", withExtension: "momd")
    guard let model = NSManagedObjectModel(contentsOf: modelURL!) else {fatalError("model not found")}
    
    //Configure NSPersistenceStoreCordinator with NSPeristentStire
    let psc = NSPersistentStoreCoordinator(managedObjectModel: model)
    let storeUrl = URL.documentURL.appendingPathComponent("CDCourses.sqlite")
    
    // TODO: Use migrations!
    //try! FileManager.default.removeItem(at: storeUrl)
    
    try! psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: nil)
    
    //Create and return NSManageObjectContext
    let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    context.persistentStoreCoordinator = psc
    return context
}
extension URL{
    static var documentURL: URL {
        return try! FileManager
            .default
            .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
}

protocol ManagedObjectContextDependentType {
    var managedObjectContext: NSManagedObjectContext! { get set}
}
