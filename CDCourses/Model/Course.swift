//
//  Course.swift
//  CDCourses
//
//  Created by 1sd on 1/22/18.
//  Copyright © 2018 isd. All rights reserved.
//

import Foundation
import CoreData

class Course: NSManagedObject {
    @NSManaged var author: String?
    @NSManaged var title: String?
    @NSManaged var releaseDate: Date?
}
