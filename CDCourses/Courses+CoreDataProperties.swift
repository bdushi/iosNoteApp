//
//  Courses+CoreDataProperties.swift
//  CDCourses
//
//  Created by 1sd on 1/22/18.
//  Copyright © 2018 isd. All rights reserved.
//
//

import Foundation
import CoreData


extension Courses {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Courses> {
        return NSFetchRequest<Courses>(entityName: "Courses")
    }

    @NSManaged public var course: String?
    @NSManaged public var r: NSDate?
    @NSManaged public var attribute2: NSObject?

}
