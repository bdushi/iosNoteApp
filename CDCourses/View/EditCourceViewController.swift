//
//  EditCourceViewController.swift
//  CDCourses
//
//  Created by 1sd on 1/22/18.
//  Copyright © 2018 isd. All rights reserved.
//

import UIKit
import CoreData

class EditCoursesViewController: UIViewController {
    
    var manageObjectContext: NSManagedObjectContext!
    var courses: Course!
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var authorField: UITextField!
    @IBOutlet weak var releaseDateField: UITextField!
    
    @IBOutlet weak var saveCourse: UIBarButtonItem!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    //Course.entityName
    override func viewDidLoad() {
        self.courses = self.courses ?? NSEntityDescription.insertNewObject(forEntityName: "Course", into: self.manageObjectContext) as! Course
        //self.courses = NSEntityDescription.insertNewObject(forEntityName: "Course", into: self.manageObjectContext) as! Course
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        self.manageObjectContext.rollback()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {
        
        self.courses.title = self.titleField.text
        self.courses.author = self.authorField.text
        self.courses.releaseDate = self.datePicker.date
        //self.courses.releaseDate = self.releaseDateField.text
        
        do {
            try self.manageObjectContext.save()
            self.dismiss(animated: true, completion: nil)
        } catch {
            let alert = UIAlertController(title: "Trouble Saving",
                                          message: "Something went wrong when trying to save the ShoutOut.  Please try again...",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",
                                         style: .default,
                                         handler: {(action: UIAlertAction) -> Void in
                                            self.manageObjectContext.rollback()
                                            self.courses = NSEntityDescription.insertNewObject(forEntityName: "Course", into: self.manageObjectContext) as! Course
                                            
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
